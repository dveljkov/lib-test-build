/**
 * @fileoverview added by tsickle
 * Generated from: lib/highlight.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild, ElementRef, ViewEncapsulation, } from "@angular/core";
export class HighlightComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.controlledContent = this.originalContent = this.content.nativeElement.textContent;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    markText(value) {
        console.log(value);
        this.controlledContent = this.originalContent.replace(new RegExp(value, "g"), `<span class="mark">${value}</span>`);
    }
}
HighlightComponent.decorators = [
    { type: Component, args: [{
                selector: "dv-highlight",
                template: `
    <input type="text" (keydown.enter)="markText($event.target.value)" />
    <div #content [hidden]="true"><ng-content></ng-content></div>
    <div [innerHTML]="controlledContent"></div>
  `,
                encapsulation: ViewEncapsulation.None,
                styles: [`
      .mark {
        background-color: yellow;
      }
    `]
            }] }
];
/** @nocollapse */
HighlightComponent.ctorParameters = () => [];
HighlightComponent.propDecorators = {
    content: [{ type: ViewChild, args: ["content", null,] }]
};
if (false) {
    /** @type {?} */
    HighlightComponent.prototype.content;
    /** @type {?} */
    HighlightComponent.prototype.originalContent;
    /** @type {?} */
    HighlightComponent.prototype.controlledContent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2hpZ2hsaWdodC8iLCJzb3VyY2VzIjpbImxpYi9oaWdobGlnaHQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNMLFNBQVMsRUFFVCxTQUFTLEVBQ1QsVUFBVSxFQUNWLGlCQUFpQixHQUNsQixNQUFNLGVBQWUsQ0FBQztBQWtCdkIsTUFBTSxPQUFPLGtCQUFrQjtJQU03QixnQkFBZSxDQUFDOzs7O0lBRWhCLFFBQVE7UUFDTixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7SUFDekYsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBVTtRQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25CLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDbkQsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxFQUN0QixzQkFBc0IsS0FBSyxTQUFTLENBQ3JDLENBQUM7SUFDSixDQUFDOzs7WUFsQ0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2dCQUN4QixRQUFRLEVBQUU7Ozs7R0FJVDtnQkFRRCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTt5QkFObkM7Ozs7S0FJQzthQUdKOzs7OztzQkFFRSxTQUFTLFNBQUMsU0FBUyxFQUFFLElBQUk7Ozs7SUFBMUIscUNBQWdEOztJQUVoRCw2Q0FBd0I7O0lBQ3hCLCtDQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgT25Jbml0LFxuICBWaWV3Q2hpbGQsXG4gIEVsZW1lbnRSZWYsXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiZHYtaGlnaGxpZ2h0XCIsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgKGtleWRvd24uZW50ZXIpPVwibWFya1RleHQoJGV2ZW50LnRhcmdldC52YWx1ZSlcIiAvPlxuICAgIDxkaXYgI2NvbnRlbnQgW2hpZGRlbl09XCJ0cnVlXCI+PG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PjwvZGl2PlxuICAgIDxkaXYgW2lubmVySFRNTF09XCJjb250cm9sbGVkQ29udGVudFwiPjwvZGl2PlxuICBgLFxuICBzdHlsZXM6IFtcbiAgICBgXG4gICAgICAubWFyayB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbiAgICAgIH1cbiAgICBgLFxuICBdLFxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxufSlcbmV4cG9ydCBjbGFzcyBIaWdobGlnaHRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBAVmlld0NoaWxkKFwiY29udGVudFwiLCBudWxsKSBjb250ZW50OiBFbGVtZW50UmVmO1xuXG4gIG9yaWdpbmFsQ29udGVudDogc3RyaW5nO1xuICBjb250cm9sbGVkQ29udGVudDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmNvbnRyb2xsZWRDb250ZW50ID0gdGhpcy5vcmlnaW5hbENvbnRlbnQgPSB0aGlzLmNvbnRlbnQubmF0aXZlRWxlbWVudC50ZXh0Q29udGVudDtcbiAgfVxuXG4gIG1hcmtUZXh0KHZhbHVlOiBhbnkpIHtcbiAgICBjb25zb2xlLmxvZyh2YWx1ZSk7XG4gICAgdGhpcy5jb250cm9sbGVkQ29udGVudCA9IHRoaXMub3JpZ2luYWxDb250ZW50LnJlcGxhY2UoXG4gICAgICBuZXcgUmVnRXhwKHZhbHVlLCBcImdcIiksXG4gICAgICBgPHNwYW4gY2xhc3M9XCJtYXJrXCI+JHt2YWx1ZX08L3NwYW4+YFxuICAgICk7XG4gIH1cbn1cbiJdfQ==