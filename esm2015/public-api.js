/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of highlight
 */
export { HighlightService } from './lib/highlight.service';
export { HighlightComponent } from './lib/highlight.component';
export { HighlightModule } from './lib/highlight.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2hpZ2hsaWdodC8iLCJzb3VyY2VzIjpbInB1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFJQSxpQ0FBYyx5QkFBeUIsQ0FBQztBQUN4QyxtQ0FBYywyQkFBMkIsQ0FBQztBQUMxQyxnQ0FBYyx3QkFBd0IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2YgaGlnaGxpZ2h0XG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvaGlnaGxpZ2h0LnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvaGlnaGxpZ2h0LmNvbXBvbmVudCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9oaWdobGlnaHQubW9kdWxlJztcbiJdfQ==