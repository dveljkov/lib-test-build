/**
 * @fileoverview added by tsickle
 * Generated from: lib/highlight.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild, ElementRef, ViewEncapsulation, } from "@angular/core";
var HighlightComponent = /** @class */ (function () {
    function HighlightComponent() {
    }
    /**
     * @return {?}
     */
    HighlightComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.controlledContent = this.originalContent = this.content.nativeElement.textContent;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    HighlightComponent.prototype.markText = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        console.log(value);
        this.controlledContent = this.originalContent.replace(new RegExp(value, "g"), "<span class=\"mark\">" + value + "</span>");
    };
    HighlightComponent.decorators = [
        { type: Component, args: [{
                    selector: "dv-highlight",
                    template: "\n    <input type=\"text\" (keydown.enter)=\"markText($event.target.value)\" />\n    <div #content [hidden]=\"true\"><ng-content></ng-content></div>\n    <div [innerHTML]=\"controlledContent\"></div>\n  ",
                    encapsulation: ViewEncapsulation.None,
                    styles: ["\n      .mark {\n        background-color: yellow;\n      }\n    "]
                }] }
    ];
    /** @nocollapse */
    HighlightComponent.ctorParameters = function () { return []; };
    HighlightComponent.propDecorators = {
        content: [{ type: ViewChild, args: ["content", null,] }]
    };
    return HighlightComponent;
}());
export { HighlightComponent };
if (false) {
    /** @type {?} */
    HighlightComponent.prototype.content;
    /** @type {?} */
    HighlightComponent.prototype.originalContent;
    /** @type {?} */
    HighlightComponent.prototype.controlledContent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2hpZ2hsaWdodC8iLCJzb3VyY2VzIjpbImxpYi9oaWdobGlnaHQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNMLFNBQVMsRUFFVCxTQUFTLEVBQ1QsVUFBVSxFQUNWLGlCQUFpQixHQUNsQixNQUFNLGVBQWUsQ0FBQztBQUV2QjtJQXNCRTtJQUFlLENBQUM7Ozs7SUFFaEIscUNBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO0lBQ3pGLENBQUM7Ozs7O0lBRUQscUNBQVE7Ozs7SUFBUixVQUFTLEtBQVU7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQ25ELElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFDdEIsMEJBQXNCLEtBQUssWUFBUyxDQUNyQyxDQUFDO0lBQ0osQ0FBQzs7Z0JBbENGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsUUFBUSxFQUFFLDZNQUlUO29CQVFELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzZCQU5uQyxtRUFJQztpQkFHSjs7Ozs7MEJBRUUsU0FBUyxTQUFDLFNBQVMsRUFBRSxJQUFJOztJQWtCNUIseUJBQUM7Q0FBQSxBQW5DRCxJQW1DQztTQW5CWSxrQkFBa0I7OztJQUM3QixxQ0FBZ0Q7O0lBRWhELDZDQUF3Qjs7SUFDeEIsK0NBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBPbkluaXQsXG4gIFZpZXdDaGlsZCxcbiAgRWxlbWVudFJlZixcbiAgVmlld0VuY2Fwc3VsYXRpb24sXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJkdi1oaWdobGlnaHRcIixcbiAgdGVtcGxhdGU6IGBcbiAgICA8aW5wdXQgdHlwZT1cInRleHRcIiAoa2V5ZG93bi5lbnRlcik9XCJtYXJrVGV4dCgkZXZlbnQudGFyZ2V0LnZhbHVlKVwiIC8+XG4gICAgPGRpdiAjY29udGVudCBbaGlkZGVuXT1cInRydWVcIj48bmctY29udGVudD48L25nLWNvbnRlbnQ+PC9kaXY+XG4gICAgPGRpdiBbaW5uZXJIVE1MXT1cImNvbnRyb2xsZWRDb250ZW50XCI+PC9kaXY+XG4gIGAsXG4gIHN0eWxlczogW1xuICAgIGBcbiAgICAgIC5tYXJrIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogeWVsbG93O1xuICAgICAgfVxuICAgIGAsXG4gIF0sXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXG59KVxuZXhwb3J0IGNsYXNzIEhpZ2hsaWdodENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBWaWV3Q2hpbGQoXCJjb250ZW50XCIsIG51bGwpIGNvbnRlbnQ6IEVsZW1lbnRSZWY7XG5cbiAgb3JpZ2luYWxDb250ZW50OiBzdHJpbmc7XG4gIGNvbnRyb2xsZWRDb250ZW50OiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuY29udHJvbGxlZENvbnRlbnQgPSB0aGlzLm9yaWdpbmFsQ29udGVudCA9IHRoaXMuY29udGVudC5uYXRpdmVFbGVtZW50LnRleHRDb250ZW50O1xuICB9XG5cbiAgbWFya1RleHQodmFsdWU6IGFueSkge1xuICAgIGNvbnNvbGUubG9nKHZhbHVlKTtcbiAgICB0aGlzLmNvbnRyb2xsZWRDb250ZW50ID0gdGhpcy5vcmlnaW5hbENvbnRlbnQucmVwbGFjZShcbiAgICAgIG5ldyBSZWdFeHAodmFsdWUsIFwiZ1wiKSxcbiAgICAgIGA8c3BhbiBjbGFzcz1cIm1hcmtcIj4ke3ZhbHVlfTwvc3Bhbj5gXG4gICAgKTtcbiAgfVxufVxuIl19