/**
 * @fileoverview added by tsickle
 * Generated from: lib/highlight.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { HighlightComponent } from './highlight.component';
var HighlightModule = /** @class */ (function () {
    function HighlightModule() {
    }
    HighlightModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [HighlightComponent],
                    imports: [],
                    exports: [HighlightComponent]
                },] }
    ];
    return HighlightModule;
}());
export { HighlightModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2hpZ2hsaWdodC8iLCJzb3VyY2VzIjpbImxpYi9oaWdobGlnaHQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUkzRDtJQUFBO0lBTStCLENBQUM7O2dCQU4vQixRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsa0JBQWtCLENBQUM7b0JBQ2xDLE9BQU8sRUFBRSxFQUNSO29CQUNELE9BQU8sRUFBRSxDQUFDLGtCQUFrQixDQUFDO2lCQUM5Qjs7SUFDOEIsc0JBQUM7Q0FBQSxBQU5oQyxJQU1nQztTQUFuQixlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEhpZ2hsaWdodENvbXBvbmVudCB9IGZyb20gJy4vaGlnaGxpZ2h0LmNvbXBvbmVudCc7XG5cblxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtIaWdobGlnaHRDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGV4cG9ydHM6IFtIaWdobGlnaHRDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIEhpZ2hsaWdodE1vZHVsZSB7IH1cbiJdfQ==