import { Injectable, ɵɵdefineInjectable, Component, ViewEncapsulation, ViewChild, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/highlight.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HighlightService {
    constructor() { }
}
HighlightService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
HighlightService.ctorParameters = () => [];
/** @nocollapse */ HighlightService.ngInjectableDef = ɵɵdefineInjectable({ factory: function HighlightService_Factory() { return new HighlightService(); }, token: HighlightService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/highlight.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HighlightComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.controlledContent = this.originalContent = this.content.nativeElement.textContent;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    markText(value) {
        console.log(value);
        this.controlledContent = this.originalContent.replace(new RegExp(value, "g"), `<span class="mark">${value}</span>`);
    }
}
HighlightComponent.decorators = [
    { type: Component, args: [{
                selector: "dv-highlight",
                template: `
    <input type="text" (keydown.enter)="markText($event.target.value)" />
    <div #content [hidden]="true"><ng-content></ng-content></div>
    <div [innerHTML]="controlledContent"></div>
  `,
                encapsulation: ViewEncapsulation.None,
                styles: [`
      .mark {
        background-color: yellow;
      }
    `]
            }] }
];
/** @nocollapse */
HighlightComponent.ctorParameters = () => [];
HighlightComponent.propDecorators = {
    content: [{ type: ViewChild, args: ["content", null,] }]
};
if (false) {
    /** @type {?} */
    HighlightComponent.prototype.content;
    /** @type {?} */
    HighlightComponent.prototype.originalContent;
    /** @type {?} */
    HighlightComponent.prototype.controlledContent;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/highlight.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HighlightModule {
}
HighlightModule.decorators = [
    { type: NgModule, args: [{
                declarations: [HighlightComponent],
                imports: [],
                exports: [HighlightComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: highlight.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { HighlightComponent, HighlightModule, HighlightService };
//# sourceMappingURL=highlight.js.map
