import { OnInit, ElementRef } from "@angular/core";
export declare class HighlightComponent implements OnInit {
    content: ElementRef;
    originalContent: string;
    controlledContent: string;
    constructor();
    ngOnInit(): void;
    markText(value: any): void;
}
