(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('highlight', ['exports', '@angular/core'], factory) :
    (global = global || self, factory(global.highlight = {}, global.ng.core));
}(this, (function (exports, core) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/highlight.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var HighlightService = /** @class */ (function () {
        function HighlightService() {
        }
        HighlightService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        HighlightService.ctorParameters = function () { return []; };
        /** @nocollapse */ HighlightService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function HighlightService_Factory() { return new HighlightService(); }, token: HighlightService, providedIn: "root" });
        return HighlightService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/highlight.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var HighlightComponent = /** @class */ (function () {
        function HighlightComponent() {
        }
        /**
         * @return {?}
         */
        HighlightComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.controlledContent = this.originalContent = this.content.nativeElement.textContent;
        };
        /**
         * @param {?} value
         * @return {?}
         */
        HighlightComponent.prototype.markText = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            console.log(value);
            this.controlledContent = this.originalContent.replace(new RegExp(value, "g"), "<span class=\"mark\">" + value + "</span>");
        };
        HighlightComponent.decorators = [
            { type: core.Component, args: [{
                        selector: "dv-highlight",
                        template: "\n    <input type=\"text\" (keydown.enter)=\"markText($event.target.value)\" />\n    <div #content [hidden]=\"true\"><ng-content></ng-content></div>\n    <div [innerHTML]=\"controlledContent\"></div>\n  ",
                        encapsulation: core.ViewEncapsulation.None,
                        styles: ["\n      .mark {\n        background-color: yellow;\n      }\n    "]
                    }] }
        ];
        /** @nocollapse */
        HighlightComponent.ctorParameters = function () { return []; };
        HighlightComponent.propDecorators = {
            content: [{ type: core.ViewChild, args: ["content", null,] }]
        };
        return HighlightComponent;
    }());
    if (false) {
        /** @type {?} */
        HighlightComponent.prototype.content;
        /** @type {?} */
        HighlightComponent.prototype.originalContent;
        /** @type {?} */
        HighlightComponent.prototype.controlledContent;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/highlight.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var HighlightModule = /** @class */ (function () {
        function HighlightModule() {
        }
        HighlightModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [HighlightComponent],
                        imports: [],
                        exports: [HighlightComponent]
                    },] }
        ];
        return HighlightModule;
    }());

    exports.HighlightComponent = HighlightComponent;
    exports.HighlightModule = HighlightModule;
    exports.HighlightService = HighlightService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=highlight.umd.js.map
